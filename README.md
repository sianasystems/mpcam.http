# MPCam.HTTP

## Dependencies
- Linux system
- Python 3.7+
    - virtualenv
- For `pystemd` library `libsystemd-dev` is needed on host.
    ```
    sudo apt install -y libsystemd-dev wget unzip
    ```
- [PyMPCam](https://github.com/siana-systems/pympcam/releases) release package.

## Setup virtual environment
From project's root:
1. Create virtual environment.
    ```
    python3 -m virtualenv env
    ```
2. Activate virtual environment.
    ```
    source env/bin/activate
    ```
3. Download PyMPCam and unzip it.
    ```
    wget https://github.com/siana-systems/pympcam/archive/refs/tags/v1.2.0.zip && unzip v1.2.0.zip && rm v1.2.0.zip
    ```
4. Build PyMPCam and install it following [documentation](https://github.com/siana-systems/pympcam#from-sources).
    ```
    cd pympcam-1.2.0 && python setup.py bdist_wheel && pip install dist/pympcam*.whl && cd ..
    ```
5. Install mpcam.http dependencies
    ```
    pip install -r requirements.txt
    ```