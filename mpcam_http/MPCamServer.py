from flask import (
    Flask,
    render_template,
    jsonify,
    request
)
from flask_classful import (
    FlaskView,
    route
)
from mpcam_http.DemoLibrary import DemoLibrary
from mpcam_http.ProcessManager import ProcessManager
from flask_socketio import SocketIO
from datetime import datetime
import netifaces
import humanize
import subprocess
from pympcam.userLed import UserLed

app = Flask(__name__)
app.config["SECRET_KEY"] = "secret!"
app.config["fd"] = None
app.config["cmd"] = ["bash"]
app.config["child_pid"] = None
key = ""

socketio = SocketIO(app)

@socketio.on('msg')
def sio_msg(data):
    print(f'got data: {data}')

icontypes = {'fa-music': 'm4a,mp3,oga,ogg,webma,wav', 'fa-archive': '7z,zip,rar,gz,tar', 'fa-picture-o': 'gif,ico,jpe,jpeg,jpg,png,svg,webp', 'fa-file-text': 'pdf', 'fa-film': '3g2,3gp,3gp2,3gpp,mov,qt', 'fa-code': 'atom,plist,bat,bash,c,cmd,coffee,css,hml,js,json,java,less,markdown,md,php,pl,py,rb,rss,sass,scpt,swift,scss,sh,xml,yml', 'fa-file-text-o': 'txt', 'fa-film': 'mp4,m4v,ogv,webm', 'fa-globe': 'htm,html,mhtm,mhtml,xhtm,xhtml'}
datatypes = {'audio': 'm4a,mp3,oga,ogg,webma,wav', 'archive': '7z,zip,rar,gz,tar', 'image': 'gif,ico,jpe,jpeg,jpg,png,svg,webp', 'pdf': 'pdf', 'quicktime': '3g2,3gp,3gp2,3gpp,mov,qt', 'source': 'atom,bat,bash,c,cmd,coffee,css,hml,js,json,java,less,markdown,md,php,pl,py,rb,rss,sass,scpt,swift,scss,sh,xml,yml,plist', 'text': 'txt', 'video': 'mp4,m4v,ogv,webm', 'website': 'htm,html,mhtm,mhtml,xhtm,xhtml'}

@app.template_filter('size_fmt')
def size_fmt(size):
    return humanize.naturalsize(size)

@app.template_filter('time_fmt')
def time_desc(timestamp):
    mdate = datetime.fromtimestamp(timestamp)
    str = mdate.strftime('%Y-%m-%d %H:%M:%S')
    return str

@app.template_filter('data_fmt')
def data_fmt(filename):
    t = 'unknown'
    for type, exts in datatypes.items():
        if filename.split('.')[-1] in exts:
            t = type
    return t

@app.template_filter('icon_fmt')
def icon_fmt(filename):
    i = 'fa-file-o'
    for icon, exts in icontypes.items():
        if filename.split('.')[-1] in exts:
            i = icon
    return i

@app.template_filter('humanize')
def time_humanize(timestamp):
    mdate = datetime.utcfromtimestamp(timestamp)
    return humanize.naturaltime(mdate)

class MPCamServer(FlaskView):
    """[summary]
    """
    route_base = '/'
    __instance = None
    __initialized = False

    def __new__(cls):
        """
        """
        if cls.__instance is None:
            cls.__instance = super(MPCamServer, cls).__new__(cls)
            cls.__instance.__initialized = False
        return cls.__instance

    def __init__(self):
        """[summary]
        """
        if(self.__initialized):
            return

        self.__initialized = True
        self.__process_manager = ProcessManager(socketio)
        self.__led = UserLed()

    @route('/')
    def index(self):
        """Renders home page of MPCam.HTTP server  
        """
        return render_template('index.html')

    def _get_default_ip_address(self) -> str:
        """Obtains the IP address of the default
        interface. Useful when there are more than
        once interfaces plugged to network
        Returns:
            str: IPv4 address
        """
        default_gw = netifaces.gateways()['default']
        iface = default_gw[netifaces.AF_INET][1]
        address = netifaces.ifaddresses(iface)[netifaces.AF_INET][0]['addr']
        return address
    
    @route('/mdns')
    def mdns_hostname(self):
        """Gets the mDNS hostname
        """
        output = subprocess.check_output(["avahi-resolve-address", self._get_default_ip_address()])
       
        hostname = output.decode('utf-8').split('\t')[1].replace('\n', '')     
   
        return jsonify({ 'hostname' : hostname })    

    @route('/ip')
    def ip_address(self):
        """Gets the IP address of the server. This route will be used only
        by Vue.js to render demos
        """
        ip_address = self._get_default_ip_address()

        return jsonify({ 'ip_address' : ip_address })
        
    @route('/led')
    def toggle(self):
        """Toggle state of system led
        """
        state = self.__led.getState("led2")
        
        if state:
            self.__led.turnOff("led2")
        else:
            self.__led.turnOn("led2")
        
        return jsonify( { 'state' : not state } )
        
    @route('/led/on')
    def turn_on_led(self):
        """Turns on system led on route call
        """
        self.__led.turnOn("led2")
        return jsonify({ 'state' : self.__led.getState("led")})

    @route('/led/off')
    def turn_off_led(self):
        """Turns off system led on route call
        """
        self.__led.turnOff("led2")
        return jsonify({ 'state' : self.__led.getState("led2")})
        
    @route('/demos/')
    def demos(self):
        """Gets the process status of all the supported demos
        """
        return jsonify(self.__process_manager._demo_library)

    @route('/demos/<demo_name>/')
    def demo_info(self, demo_name):
        """Gets the process status of the selected demo

        Args:
            demo_name (str): Requested demo
        """
        return jsonify(self.__process_manager._demo_library[demo_name])    

    @route('/demos/<demo_name>/output')
    def demo_output(self, demo_name):
        """[summary]

        Args:
            demo_name ([type]): [description]

        Returns:
            [type]: [description]
        """
        pass
        #return redirect(self.__process_manager.demoOutput(demo_name))

    @route('/demos/<demo_name>/start')
    def start_demo(self, demo_name):
        """Starts the demo

        Args:
            demo_name (str): Demo to start
        """
        return jsonify(self.__process_manager.start(demo_name, request.args))

    @route('/demos/<demo_name>/stop')
    def stop_demo(self, demo_name):
        """Stops the demo

        Args:
            demo_name (str): Demo to start
        """
        return jsonify(self.__process_manager.stop(demo_name))

    @route('/version')
    def version(self):
        """Returns the MPCam release version
        """
        version = subprocess.check_output(['lsb_release', '-r']).decode().replace("Release:\t", "").replace("\n", "")
        return jsonify({ "version" : version })

    def startServer(self) -> None:
        """Serves HTML static files
        """
        socketio.run(app, port=80, debug=False, host='0.0.0.0')

MPCamServer.register(app)
