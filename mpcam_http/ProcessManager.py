from os import popen
import subprocess
import threading
from flask_socketio import SocketIO

EVT = 'demo_evt'

class DemoRunner(threading.Thread):
    def __init__(self, demo_name, sio:SocketIO):
        threading.Thread.__init__(self)
        self.sio = sio

        self.path = '/usr/local/mpcam-demos/'
        self.cmd = ['python3', '-u']
        self.start_key = '>> FPS:'

        if demo_name == 'object-detection':
            self.path += 'mpcam-demo-detector/'
            self.cmd += ['detect_camera.py']
        elif demo_name == 'posenet':
            self.path += 'mpcam-demo-posenet/'
            self.cmd += ['pose_camera.py']
        elif demo_name == 'live-tracker':
            self.path += 'mpcam.stura.vtracker/'
            self.cmd += ['vtracker.py', '--config', 'mpcam_live']
            self.start_key = 'vtracker - INFO - Frames skipped'
        elif demo_name == 'offline-tracker':
            self.path += 'mpcam.stura.vtracker/'
            self.cmd += ['vtracker.py', '--config', 'mpcam_offline']


    def run(self):
        self.process = subprocess.Popen(self.cmd, cwd=self.path, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        self.sio.emit(EVT, "load")

        validLineCount = 0
        for line in iter(self.process.stdout.readline, b''):
            if self.start_key in line.decode():
                validLineCount += 1
                if validLineCount == 2:
                    self.sio.emit(EVT, "start")
            self.sio.emit('stdout', line.decode())
        self.process.stdout.close()
        return self.process.wait()

    def stop(self):
        self.process.terminate()
        self.join(10)

class ProcessManager:
    """[summary]
    """
    def __init__(self, socketio:SocketIO):
        """[summary]
        """
        self.thread = None
        self.sio = socketio
        self._demo_library = {
            'camera-stream' : {
                'running' : False
            },
            'posenet' : {
                'running' : False
            },
            'object-detection' : {
                'running' : False
            },
            'live-tracker' : {
                'running' : False
            },
            'offline-tracker' : {
                'running' : False
            }
        }

    def start(self, demo_name, demo_args):
        """[summary]

        Args:
            demo_name ([type]): [description]
        """
        response = {
            'operation_response' : False,
            'operation_message' : "Demo already running"
        }
        if self._demo_library[demo_name]['running']:
            return response

        if not self.__start_process(demo_name, demo_args):
            response['operation_message'] = "Demo could not be run"
            return response

        for key in self._demo_library:
            self._demo_library[key]['running'] = False
        self._demo_library[demo_name]['running'] = True

        return {
            'operation_response' : True,
            'operation_message' : "Demo started succesfuly"
        }

    def __start_process(self, demo_name, demo_args):
        """[summary]

        Args:
            demo_name ([type]): [description]
        """
        self.thread = DemoRunner(demo_name, self.sio)
        self.thread.start()
        return self.thread.is_alive()

    def stop(self, demo_name):
        """[summary]

        Args:
            demo_name ([type]): [description]

        Returns:
            [type]: [description]
        """
        response = {
            'operation_response' : False,
            'operation_message' : 'Demo already stopped'
        }
        if not self._demo_library[demo_name]['running']:
            return response

        if not self.__stop_process():
            response['operation_message'] = "Demo could not be stopped"
            return response

        self._demo_library[demo_name]['running'] = False

        return {
            'operation_response' : True,
            'operation_message' : 'Demo stopped succesfuly'
        }

    def __stop_process(self):
        """[summary]

        Args:
            demo_name ([type]): [description]

        Returns:
            [type]: [description]
        """
        try:
            self.sio.emit(EVT, "stop")
            self.thread.stop()
            return True
        except Exception as e:
            print(e)
            return False

    def demoOutput(self, demo_name):
        """[summary]

        Args:
            demo_name ([type]): [description]
        """
        if demo_name == "live-tracker" or demo_name == "offline-tracker":
            return None