var ignore = false

const app = new Vue({ // Again, vm is our Vue instance's name for consistency.
    el: '#app',
    delimiters: ['[[', ']]'],
    data: {
        greeting: 'Hello, Vue!',
        view: 'demos',
        selectedDemo: '',
        currentRunningDemo: '',
        selectedHeader: '',
        ipAddress: '',
        version: '',
        hostname: '',
        stdout: ''
    },
    methods: {
        toggleButton() {
            $.getJSON('/led', (response) => {
                console.log("System LED toggled")
	    })
        },
        thresholdModified (threshold){
            this.objectDetectionThreshold = threshold/100
        },
        changeView: function(view) {
            this.view = view;
            this.$nextTick(() => {
                term.fit()
                term.resize(20, 20)
                term.fit()
            })
        },
        selectDemo: function(demo) {
            this.selectedDemo = demo
            if (demo == 'posenet') {
                this.selectedHeader = 'Posenet demo'
            }
            else if (demo == 'object-detection') {
                this.selectedHeader = 'Object detection demo'
            }
            else if (demo == 'live-tracker') {
                this.selectedHeader = 'Live tracker demo'
            }
            else if(demo == 'offline-tracker') {
                this.selectedHeader = 'Offline tracker demo'
            }
        },
        startDemo: function(demo) {
            if(demo != 'object-detection'){
                $.getJSON('/demos/' + demo + "/start", (response) => {
                    if(response.operation_response == true){
                        this.currentRunningDemo = demo
                        console.log("Selected " + demo)
                    }
                    console.log(response.operation_message)
                })
            }
            else {
                $.getJSON('/demos/' + demo + "/start?threshold=" + this.objectDetectionThreshold, (response) => {
                    if(response.operation_response == true){
                        this.currentRunningDemo = demo
                        console.log("Selected " + demo)
                    }
                    console.log(response.operation_message)
                })
            }
        },
        stopDemo: function(demo) {
            $.getJSON('/demos/' + demo + "/stop", (response) => {
                if(response.operation_response == true){
                    this.currentRunningDemo = ""
                    console.log("Stopped " + demo)
                }
                console.log(response.operation_message)
            })
        }
    },
    mounted: function() {
        $.getJSON('/demos', (response) => {
            for(var key in response) {
                if(response[key].running == true) {
                    this.currentRunningDemo = key
                }
            }
        })
        $.getJSON('/ip', (response) => {
            this.ipAddress = response.ip_address
        })
        $.getJSON('/version', (response) => {
            this.version = response.version
        })
        $.getJSON('/mdns', (response) => {
            this.hostname = response.hostname
        })
    }
})

