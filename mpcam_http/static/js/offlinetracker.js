Vue.component('offline-tracker-configuration', {
    data: function () {
        return { }
    },
    template: "<div id='container'><h4 style='text-align: center'>Offline tracker doesn't have any configurable parameter</h4></div>"
})

Vue.component('offline-tracker-results', {
    data: function () {
        return {
            streamAddress: '',
            txt: '',
            enabled: false,
            loading: false
        }
    },
    template: `
    <div class="container">
    <div class="collapse" id="consoleDiv">
    <div class="card card-body">
    <textarea id=console class="form-control" rows="10">{{ txt }}</textarea>
    </div>
    </div>
    <h2>Stream</h2>
    <img src=static/img/spinner.gif v-show='loading'></img>
    <iframe id='objectDetectionFrame' :src='streamAddress' style="overflow:hidden;height:580px;width:740%;" frameBorder="0"></iframe>
    </div>
        `,
    created() {
        const socket = io.connect('http://' + document.domain + ':' + location.port);
        socket.on('demo_evt', (evt) =>{
            console.log("Event: " + evt)
            switch(evt) {
                case 'load':
                    this.streamAddress = '';
                    this.txt = ''
                    this.loading = true;
                    this.enabled = false;
                    break;
                case 'start':
                    if (this.enabled == false) {
                        $.getJSON('/ip', (response) => {
                            this.streamAddress = "http://" + response.ip_address + ":8081/video.mjpg"
                        });
                        this.enabled = true;
                        this.loading = false;
                    }
                    break;
                case 'stop':
                    this.streamAddress = '';
                    this.loading = false;
                    this.enabled = false;
            }
        });

        socket.on('stdout', (data) => {
            this.txt += data;
            document.getElementById("console").scrollTop = document.getElementById("console").scrollHeight
        });
    }
})