import setuptools
import mpcam_http

setuptools.setup(
    name="mpcam.http",
    version=mpcam_http.VERSION,
    author="Marcelo Cardenas",
    author_email="marcelo@siana-systems.com",
    description="Python's HTTP service package for MPCam",
    url="https://bitbucket.org/sianasystems/mpcam.http/",
    packages=setuptools.find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)